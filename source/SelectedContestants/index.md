---
title: Selected Contestants
date: 2019-12-29 07:57:56
---

### For the Next Pwn Race...

| Rank | Name   | Id   |
| -----|:------:| ----:|
| 1    | Stalemate   | Cadet0x30 |
| 2    | Grankenox    | Rebel0x4 |
| 3    | Suvaditya Sur    | Rebel0x02 |
| 4    | Pranav   | Padawan0xc |
