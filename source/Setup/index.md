---
title: Setup
date: 2019-12-26 22:28:25
---

## Type Race

* No setup required. Come to the booth and play.

## Pwn and Crypto race

* There is no prior setup required. All these instructions will also be given to you before the race starts

* When the race is about to start the participants will be provided with a URL.

* The participant has to select a screen name for himself/herself.

* The participant has to share the window on which he/she is working, so that it can be his/her progress and methods can be live streamed at the stall.
