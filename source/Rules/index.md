---
title: Rules
date: 2019-12-26 22:28:35
---

* For any particular race, an individual is eligible for only one prize. They can however participate multiple times.

* For the type race event it is necessary that the individual is physically present at the speedpatrol booth.

* All the races are individual events and if a collaboration is detected, the individual will be disqualified and made ineligible for the prizes.

* For the type race, the prizes are random and amazing swag!

## Specifically for non type race event:

* Each of the races will have 4 contestants.

* For participation the, interested contents have to provide their name and id at the time of call for participants. 4 random contestants will be chosen from this pool.

* Each of the participants will be assigned separate rooms and will be given instruction on how to setup for the stream. The instructions can also be found [here]()

* Participants have to use their own computers and no machine will be provided.

* In the unlikely event of us running out of time before any of the contestants finish the race, we reserve the right to award the prize to the person who has accomplished the most regarding the challenge, given on the sole discretion of the organizers.

* Each winner will be get a custom designed drinking mug as a prize.
