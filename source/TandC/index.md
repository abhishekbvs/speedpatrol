---
title: Terms and Conditions
date: 2019-12-26 22:29:02
---

* By taking part in the competition you acknowledge and agree to abide by the following terms and conditions.

* For any particular race an individual is eligible for only one prize.

* The prizes are as stated and no cash or any other alternatives will be offered. Prizes are subject to availability and we reserve the right to substitute the prize with another of equal value without prior notice.

* The organizers decision in all matters related with the contest, including the results, will be final and no negotiation in this regard will be entertained.

* The terms and conditions are liable to change without prior notice according to the organizers discretion.
